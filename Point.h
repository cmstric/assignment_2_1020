/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the header file for the class Point, which will declare all
 * the functions to be used for the class
 */
#ifndef POINT_H
#define POINT_H

class Point
{
	private:
		double x;
		double y;
	public:
		/*default constructor*/
		Point();

		/*setters*/
		void setX(double);
		void setY(double);
		void setPoint(double, double);

		/*getters*/
		double getX();
		double getY();
};
#endif
