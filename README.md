# README #

This program will take in 6 points, 3 of 2 different triangles, determine which points are inside the triangles and change their colors respectivley so that triangles are displayed.

### Problems encountered & solutions###
* 1) A problem I encountered was how to determine which triangle was inside 
	  the other, I solved this by using an area formula so I know the smaller
	  triangle had to be inside the larger.
*
* 2) I had in more for loop <= 1, which caused my triangles to print an element
	  off, and for the longest time I could not figure out what was wrong, I 
	  rectified this eventually.


### Thoughts on the assignment ###
*	I really enjoyed this assignment, it is very rewarding to see immediate
*	results. What made this assinment so interesting to me is the fact that it
	I like seeing the triangles work with all different cases. However,
	it would be nice to work with something other than ppm images and have
	a program that accomplishes other tasks than ppm images.