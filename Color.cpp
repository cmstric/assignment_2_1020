/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the cpp file that will go with the Color.h file
 */

#include "Color.h"

Color::Color(){
	red = 0;
	green = 0;
	blue = 0;
}

/*Setters*/
void Color::setRed(int r){
	red = static_cast<unsigned char>(r);
}

void Color::setGreen(int g){
	green = static_cast<unsigned char>(g);
}

void Color::setBlue(int b){
	blue = static_cast<unsigned char>(b);
}

void Color::setColors(int r, int g, int b){
	red = static_cast<unsigned char>(r);
	green = static_cast<unsigned char>(g);
	blue = static_cast<unsigned char>(b);
}


/*Getters*/
unsigned char Color::getRed(){
	return red;
}

unsigned char Color::getGreen(){
	return green;
}

unsigned char Color::getBlue(){
	return blue;
}
