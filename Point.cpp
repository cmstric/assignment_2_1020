/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the cpp file that will go with the Point.h file
 */

#include "Point.h"
Point::Point(){
	x = 0;
	y = 0;
}
/*Setters*/
void Point::setX(double xCoord){
	x = xCoord;
}
void Point::setY(double yCoord){
	y = yCoord;
}
void Point::setPoint(double xCoord, double yCoord){
	x = xCoord;
	y = yCoord;
}

/*Getters*/
double Point::getX(){
	return x;
}
double Point::getY(){
	return y;
}
