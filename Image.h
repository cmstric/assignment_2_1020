/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the header file for the class Image, which will declare all
 * the functions to be used for the class
 */
#ifndef IMAGE_H
#define IMAGE_H
#include <fstream>
#include <iostream>
#include <string>
#include "Pixel.h"
#include "Header.h"

class Image
{
	private:
		Pixel pix;
		Header hdr;

  public:
		void setHeader(int w, int h);
		void printPixel(ofstream &out);
		void printHeader(ofstream &out);
		void setPixel(bool colorChoice);
		void setRGB(int r, int g, int b);
		void setPoint(double x1, double y1);
   

};
#endif
