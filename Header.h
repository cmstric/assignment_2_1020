/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the header file for the class Header, which will declare all
 * the functions to be used for the class
 */
#ifndef HEADER_H
#define HEADER_H
#include <string>
#include <fstream>
using namespace std;


class Header
{
	private:
		string magic;
		int width;
		int height;
		int maxRGB;

	public:
		/*default constructor*/
		Header();

		/*setters*/
		void setMagic(string);
		void setWidth(int);
		void setHeight(int);
		void setMaxRGB(int);

		/*print*/
		void printHeader(ofstream&); // Start back here
};

#endif
