/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the driver file that will take all of the classes and use them to 
 * print out the correct image.
 */
#include <iostream>
#include <stdlib.h>
#include "Image.h"
using namespace std;

// Prototype for the function that will test the points
bool checkPoint(Point p[3], Point test);

int main(int argc, char *argv[])
{
  	ofstream outPut(argv[2]);
  	ifstream inPut(argv[1]);
   // Initialize the variables 
  	Image img;
  	Point point1[3];
  	Point point2[3];
  	Point temp[3];
   Point test;
  	int i, j, a1, a2;
   double x, y;
  	int height, width;

 	if(argc != 3)
	{
		cout << "USAGE ERROR:  ./executable outPutFileName";
		exit(EXIT_FAILURE);
  	}
  	if(inPut.fail())
	{
		cout << argv[1] << " did not open successfully\n";
  	}

  	if(outPut.fail())
  	{
		cout << argv[2] << " did not open successfully\n";
  	}
  
  	/*read the data from the input file here*/
	 // Get the height and width
    inPut >> height >> width;
    
    // Populate point1 with x and y terms
    for (int i = 0; i < 3; ++i)
    {
      inPut >> x;
      inPut >> y;
      point1[i].setPoint(x, y);
    }
	 // Populate point2 with x and y terms
    for (int i = 0; i < 3; ++i)
    {
      inPut >> x;
      inPut >> y;
      point2[i].setPoint(x, y);
    }
  	 // Set width and height of the image
    img.setHeader(width, height);
    img.printHeader(outPut);
    
    // test the areas of the triangle
    a1 = point1[0].getX()*point1[1].getY()+point1[1].getX()*point1[2].getY()+\
         point1[2].getX()*point1[0].getY()-point1[0].getX()*point1[2].getY()-\
         point1[1].getX()*point1[0].getY()-point1[2].getX()*point1[1].getY();

    a2 = point2[0].getX()*point2[1].getY()+point2[1].getX()*point2[2].getY()+\
         point2[2].getX()*point2[0].getY()-point2[0].getX()*point2[2].getY()-\
         point2[1].getX()*point2[0].getY()-point2[2].getX()*point2[1].getY();
    
    if(a1 < a2)
    {
    	for(i = 0; i < 3; i++)
    	{
    		temp[i] = point1[i];
    		point1[i] = point2[i];
    		point2[i] = temp[i];
    	}
    }
  	/*using nested for loops call the client function to test if
    *the given point is within the parameters of the triangle.  
    *Using the instance of image set the color of the pixels RGB
    *channels and then call the function to print the Pixel to 
    *the output file.*/
	 // Test the points if they are in the triangle/s or not
    for(i = 0; i < height; i++)
    {
      test.setY(i);
      for (j = 0; j < width; j++)
      {
        test.setX(j);
        // Check the points in the for loop
        img.setPixel(checkPoint(point1, test));
		  // Inner Triangle
		  if(checkPoint(point2, test))
		  {
			  img.setRGB(255, 255, 255);
		  }
        img.printPixel(outPut);
      }
    }
   // Close files originally open
  	inPut.close();
  	outPut.close();

  	return 0;
}

// Function to test if a point in in a triangle
bool checkPoint(Point p[3], Point test){
   // Initalize a,b, and c
   double a = 0;
   double b = 0;
   double c = 0;
   double x = test.getX();
   double y = test.getY();
   double x1 = p[0].getX();
   double x2 = p[1].getX();
   double x3 = p[2].getX();
   double y1 = p[0].getY();
   double y2 = p[1].getY();
   double y3 = p[2].getY();

   // Find the a value
   a = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) /\
       ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));

   // Find the b value
   b  = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) /\
        ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));

   // Find the c value
   c = 1.0 - a - b;

   if (a <= 1.0 && a >= 0.0 && b <= 1.0 && b >= 0.0 && c <= 1.0 && c >= 0.0) {
      return true;
   }
   else
      return false;
}
