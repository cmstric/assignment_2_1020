/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the header file for the class Color, which will declare all
 * the functions to be used for the class
 */
#ifndef COLOR_H
#define COLOR_H

class Color
{
  private:
	  unsigned char red;
	  unsigned char green;
	  unsigned char blue;
  public:
	  /*Default constructor*/
	  Color();

	  /*Setters*/
	  void setRed(int);
	  void setGreen(int);
	  void setBlue(int);
	  void setColors(int, int, int);
	  /*Getters*/
	  unsigned char getRed();
	  unsigned char getGreen();
	  unsigned char getBlue();
};

#endif
