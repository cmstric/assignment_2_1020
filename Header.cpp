/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the cpp file that will go with the Header.h file
 */

 #include "Header.h"

/*Default constructor*/
 Header::Header(){
    width = 0;
    height = 0;
    maxRGB = 255;
    magic = "P6";
}

/*Setters*/
void Header::setMagic(string s){
   magic = s;
}

void Header::setWidth(int w){
   width = w;
}

void Header::setHeight(int h) {
   height = h;
}

void Header::setMaxRGB(int m) {
   maxRGB = m;
}

/*Print Header*/
void Header::printHeader(ofstream& out) {
   out << magic << "\n" << width << " " <<  height << " " << maxRGB << "\n";
}
