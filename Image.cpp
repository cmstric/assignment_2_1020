/*
 * Carl Strickland
 * cmstric
 * CPSC 1020 Assignment 2
 * 3/19/17 due date
 * This is the cpp file that will go with the Image.h file
 */

#include "Image.h"

void Image::setHeader(int w, int h){
	hdr.setWidth(w);
	hdr.setHeight(h);
}

void Image::printHeader(ofstream &out){
	hdr.printHeader(out);
}

void Image::printPixel(ofstream &out){
	pix.printRGB(out);
}

void Image::setPixel(bool colorChoice){
	pix.chooseColor(colorChoice);
}

void Image::setRGB(int r, int g, int b){
	pix.setColor(r, g, b);		
}

void Image::setPoint(double x, double y){
	pix.setCoord(x, y);
}
